const functions = require('./functions')

test('Bogus test that means nothing', () => {
  expect(functions.add(2, 2)).toBe(4)
})
