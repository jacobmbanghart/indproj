// https://docs.cypress.io/api/introduction/api.html
const email = ''
const password = ''
describe('My First Test', () => {
  it('Visits the app root url', () => {
    cy.visit('/')
    cy.contains('div', 'Login')
    cy.get('#inputemail')
      .type(`${email}`)
      .should('have.value', `${email}`)
    cy.get('#inputpassword')
      .type(`${password}`)
    cy.contains('button', 'Login!').click()
    cy.contains('h3', 'Watch Lists:')
  })
})
