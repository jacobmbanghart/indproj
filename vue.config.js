module.exports = {
  transpileDependencies: [
    'vuetify'
  ],

  pwa: {
    name: 'JacobBanghart',
    themeColor: '#000000',
    workboxPluginMode: "InjectManifest",
    workboxOptions: {
      swSrc: "src/service-worker.js"
    }
  },
  outputDir: "./dist"
}
