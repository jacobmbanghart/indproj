import Vue from 'vue'
import App from './App.vue'
import store from './store';
import router from './router'
import vuetify from './plugins/vuetify'
import { createProvider } from './vue-apollo'
import './registerServiceWorker'
Vue.config.productionTip = false

new Vue({
  vuetify,
  router,
  store,
  apolloProvider: createProvider(),
  render: h => h(App)
}).$mount('#app')
